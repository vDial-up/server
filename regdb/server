#!/usr/bin/python3
# vDial-up Server - Registeration Server
# Copyright (C) 2015 - 2016 Nathaniel Olsen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random
import json
from time import sleep
import socket
import socketserver
from subprocess import call
from os import name, getpid, urandom
import hashlib
import string
import base64
import struct
from multiprocessing import Process

def clear():
    if name == 'nt':
        call('cls')
    else:
        call('clear')

version = "0.9.2"
config = json.load(open("config.json"))
regdB = json.load(open("regdB.json"))
servdB = json.load(open("servdB.json"))
cache = json.load(open("cache.json"))

def start_v6(t_v6):
    t_v6.serve_forever()

def keynumcheck(key, self):
    new_vNumber = random.randint(100000001, 999999999)
    try: # If this fails, it means the number doesn't exists.
        prevnum = regdB[str(new_vNumber)]
    except KeyError:
        regdB[str(new_vNumber)] = {}
        h = hashlib.sha512() # Hash the keys, server-sided. In case of a dB leak or a breach.
        bytes_string = key.encode('utf-8')
        h.update(bytes_string)
        regdB[str(new_vNumber)]
        regdB[str(new_vNumber)]['Key'] = h.hexdigest()
        json.dump(regdB, open('regdB.json', 'w'), indent=2)
        self.request.send(("CONFIG: vNumber %s\n" % (new_vNumber,)).encode('utf-8'))
        self.request.send(("CONFIG: Key %s\n" % (key,)).encode('utf-8'))
        self.request.send(("TOCLIENT: Your new vNumber is %s\n" % (new_vNumber,)).encode('utf-8'))
        self.request.send(("TOCLIENT: Welcome to vDial-up, your registeration has completed!\n".encode('utf-8')))
        self.request.send(("TOCLIENT: Unfourtantly, vDial-up is still in development, so there is no directory server yet. But you can still access servers (if you have the server's vNumber).\n".encode('utf-8')))

def md5():
    hash_md5 = hashlib.md5()
    with open("server", "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    config['MD5SUM'] = hash_md5.hexdigest()
    json.dump(config, open('config.json', 'w'), indent=2)
    return

def startup():
    md5()
    clear()
    logo_file = open('logo', 'r')
    logo = logo_file.read()
    print(logo)
    print("Project vDial-up Server V%s" % (version))
    print('PID: ' + str(getpid()))
    sleep(2)
    print("Starting Registeration Server...")
    try:
        t = ThreadedTCPServer((config['IP'],5000), service)
        if config['ipv6_support'] == True:
            t_v6 = ThreadedTCPServer_v6((config['IPv6'],5000), service)
        print("Registeration server started!")
        if config['ipv6_support'] == True:
            Process(target=start_v6, args=[t_v6]).start()
        t.serve_forever()
    except KeyboardInterrupt:
        print("Shutting down.")
        t.shutdown()

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

class ThreadedTCPServer_v6(socketserver.ThreadingMixIn, socketserver.TCPServer):
    address_family = socket.AF_INET6

class service(socketserver.BaseRequestHandler):
    def send_msg(self, msg):
        # Prefix each message with a 4-byte length (network byte order)
        msg = struct.pack('>I', len(msg)) + str.encode(msg)
        self.request.send(msg)

    def recv_msg(self):
        # Read message length and unpack it into an integer
        raw_msglen = service.recvall(self, 4)
        if not raw_msglen:
            return None
        msglen = struct.unpack('>I', str.encode(raw_msglen))[0]
        return service.recvall(self, msglen)

    def recvall(self, n):
        # Helper function to recv n bytes or return None if EOF is hit
        data = ''
        while len(data) < n:
            packet = (self.request.recv(n - len(data)).decode('utf-8'))
            if not packet:
                return None

            data += packet
        return data

    def handle(self):
        cache[self.client_address[0]] = {}
        json.dump(cache, open('cache.json', 'w'), indent=2)
        print("New connection from %s" % (self.client_address[0]))
        while 1:
            self.data = service.recv_msg(self)
            #self.data = self.request.recv(1024).strip()
            #if not self.data:
                #break
            convertdata = self.data
            try:
                print("{}: {}".format(self.client_address[0], convertdata.upper()))
            except AttributeError:
                pass

            if convertdata == "SERVPING":
                service.send_msg(self, "PONG")
            if convertdata == "INITPING":
                service.send_msg(self, "PONG")
            if convertdata == "MD5SUMCHECK":
                service.send_msg(self, "MD5SUM: {}".format(config['MD5SUM']))

            if convertdata == "CLIENTREGISTER":
                if cache[self.client_address[0]]["vNumber"] == "000000000":
                    length = 1024
                    chars = string.ascii_letters + string.digits + '!@#$%^&*()'
                    random.seed = (urandom(2048))
                    key = ''.join(random.choice(chars) for i in range(length))
                    keynumcheck(key, self)
                if cache[self.client_address[0]]["vNumber"] == "":
                    self.request.send(("You have not identified yourself to RegServ...".encode('utf-8')))
                else:
                    self.request.send(("TOCLIENT: You have already registered!".encode('utf-8')))

            if convertdata.split()[0] == "CONNECT":
                if cache[self.client_address[0]]["vNumber"] != "000000000":
                    pass # I will work on this when I come across that bridge.
                if vNumber == "":
                    service.send_msg(self, "You have not identified yourself to RegServ...")
                else:
                    service.send_msg(self, "You have not registered to services yet...")

            if convertdata.split()[0] == "VNUMBER:":
                cache[self.client_address[0]]["vNumber"] = str(convertdata.split()[1])
                json.dump(cache, open('cache.json', 'w'), indent=2)

            if convertdata.split()[0] == "KEY:":
                h = hashlib.sha512
                bytes_string = convertdata.split()[0].encode('utf-8')
                h.update(bytes_string)
                cache[self.client_address[0]]["Key"] = h.digest()
                json.dump(cache, open('cache.json', 'w'), indent=2)

            if convertdata.split()[0] == "PERMITCLIENTCONFIG:":
                if convertdata[1] == "True":
                    cache[self.client_address[0]]["AllowAccessToClientConfig"] = True
                    json.dump(cache, open('cache.json', 'w'), indent=2)
                else:
                    cache[self.client_address[0]]["AllowAccessToClientConfig"] = False
                    json.dump(cache, open('cache.json', 'w'), indent=2)

            if convertdata.split()[0] == "INIT":
                vNumber == cache[self.client_address[0]]["vNumber"]
                if cache[self.client_address[0]]["Key"] == regdB[vNumber][Key]:
                    self.request.send(("Identification has succeeded."))
                    cache[self.client_address[0]]["ID"] = True
                    json.dump(cache, open('cache.json', 'w'), indent=2)
                else:
                    self.request.send(("Error: The key you have on file does not match with ours, did you change the vNumber or key?".encode('utf-8')))

            if convertdata[0] == "Serv_MD5SUM_Check":
                pass # I will work on this when I come across that bridge.

            if convertdata[0] == "SERVLIST":
                pass # I will work on this when I come across that bridge.


if __name__ == "__main__":
    startup()
