#vDial-up Server

vDial-up Server Copyright (C) 2015 - 2016 Nathaniel Olsen

###Warning: vDial-up is still in development, so some features might be missing, incomplete, or otherwise, not work at all. Use at your own risk.

####Requirements: Python 3.0 or newer, a constant internet connection, Minimum internet connection speed of 100Kbps download/upload.

Project vDial-up, or vDial-up, is a virtual dial-up emulator that connects to the internet, but emulate the internet with speeds as if you connected with Dial-up today.

IRC Channel: #vDial-up @ freenode

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
